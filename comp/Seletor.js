import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';

export default function Seletor(props) {
  const [num, setNum] = useState(0);
  
  useEffect(()=>{
    props.onChange(num);
  },[num]);

  const mais = () => {
    if (num == props.max) {
      setNum(props.min);
    } else {
      setNum(num + 1);
    }
  }
  const menos = () => {
    if (num == props.min) {
      setNum(props.max);
    } else {
      setNum(num - 1);
    }
  }
  return (
    <View>
      <TouchableOpacity onPress={mais}>
        <Text style={styles.seta}>▲</Text>
      </TouchableOpacity>
      <Text style={styles.text}>{('0'+num).slice(-2)}</Text>
      <TouchableOpacity onPress={menos}>
        <Text style={styles.seta}>▼</Text>
      </TouchableOpacity>
    </View>)
}

Seletor.defaultProps = {
  min:0,
  max:59,
  onChange: ()=>{},
}

const styles = StyleSheet.create({
  cont: {
  },
  touch: {},
  text: {
    fontSize: 60,
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'center'
  },
  seta: {
    fontSize: 40,
    color: '#fff',
    textAlign: 'center',
    opacity: 0.4
  },
});